import React from "react";
import ReactDOM from "react-dom";
import App from "./App.jsx";
// import * as _ from "lodash";
// import moment from "moment";
// import "./main.css";
// import * as serviceWorker from "./serviceWorker";
// import {add} from './hashtest/add'
// import {sub} from './hashtest/sub'

// function App:any(){
//   return null
// }

// const addResult = add(11 + 22);
// const subResult = sub(33 - 44);

// console.log(addResult, subResult);
// console.log(_.join([addResult, subResult], ':'));

let root = document.getElementById("root");
if (root?.hasChildNodes()) {
  ReactDOM.hydrate(<button />, root);
} else {
  ReactDOM.render(
    <React.StrictMode>
      <App/>
    </React.StrictMode>,
    root
  );
}

// _.add(1,3)

// console.log(moment.isDate);
// moment.locale('zh-cn');
// const date = moment().format('LL');
// console.log(date);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
