import React, { Component } from "react";

export default class DebouceTestClass extends Component {
  constructor(props) {
    super(props);
    this.state = { date: new Date(), a: "init" };
    this.timer;
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date(),
    });
  }
  onChangeHandle = (e) => {
    console.log(this.timer);
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.setState({
        a: e.target.value,
      });
      console.log("onchange", e, this.timer, this.timerID, this.state.date);
    }, 2000);
  };
  render() {
    console.log("render", this.state.date);
    return (
      <>
        <h2>date：{this.state.date.toLocaleTimeString()}.</h2>
        <h2>a：{this.state.a}</h2>
        <h2>timer：{this.timer}</h2>
        <h2>timerID：{this.timerID}</h2>
        <input type="text" onChange={this.onChangeHandle}></input>
      </>
    );
  }
}
