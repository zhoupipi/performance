import React, { Suspense, lazy } from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';

// const Card = lazy(() => import("./Card"));
import model from "./model.js";
import Card from './Card';

const styles = (theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignContent: "center",
    flexWrap: "wrap",
    marginTop: "2rem",
  },
  title: {
    display: "block",
    width: "100%",
    paddingLeft: 40,
    paddingRight: 40,
    fontFamily: "Long Cang",
    fontWeight: 400,
    fontStyle: "normal",
    lineHeight: "1.40455",
    marginLeft: "auto",
    marginRight: "auto",
    textAlign: "left",
  },
});
// relay
class Home extends React.Component {
  render() {
    let cards = [];
    for (let i = 0; i < 100; i++) {
      cards.push(
        model.map((item) => (
          <Suspense key={item.name+'sus'} fallback={<div key={item.name+'loading'}>正在加载...</div>}>
            <Card
              key={item.name}
              image={item.image}
              title={item.name}
              route={item.route}
              description={item.body}
            />
          </Suspense>
        ))
      );
    }
    return (
      <main className={this.props.classes.root}>
        <title className={this.props.classes.title}>
          <span>看看字体的样式</span>
        </title>
        {cards}
      </main>
    );
  }
}

export default withStyles(styles)(Home);
