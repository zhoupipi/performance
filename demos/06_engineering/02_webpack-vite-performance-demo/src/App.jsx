import React from "react";
import { createTheme, MuiThemeProvider } from "@material-ui/core/styles";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from "./Header";
import Home from "./Home";
import DebouceClass from "./DebouceClass";
import DebouceHook from "./DebouceHook";
import loadable from "@loadable/component";

const primary = "#1451c9";

const theme = createTheme({
  palette: {
    primary: {
      main: primary,
      contrastText: "#fff",
    },
    secondary: {
      main: "#000000",
      contrastText: primary,
    },
  },
});

// 使用React-Loadable动态加载组件
const LoadableAbout = loadable(() => import("./About.jsx"), {
  fallback: "<div>loading...</div>",
});

class App extends React.Component {
  constructor(props) {
    super(props);
    // this.calculatePi(1500); // 测试密集计算对性能的影响
    // test(); // 测试函数lazy parsing, eager parsing
  }

  calculatePi(duration) {
    const start = new Date().getTime();
    while (new Date().getTime() < start + duration) {
      // TODO(Dereck): figure out the Math problem
    }
  }

  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Router>
          <Switch>
            <React.Fragment>
              <Header />
              <Route exact path="/" component={Home} />
              <Route path="/about" component={LoadableAbout} />
              <Route path="/debouceclass" component={DebouceClass} />
              <Route path="/deboucehook" component={DebouceHook} />
            </React.Fragment>
          </Switch>
        </Router>
      </MuiThemeProvider>
    );
  }
}

export default App;
