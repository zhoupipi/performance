const model = [
  {
    name: `地球`,
    image: `https://cdn.pixabay.com/photo/2021/03/30/19/22/orange-breasted-sunbird-6137612__480.jpg`,
    body: "鸟",
  },
  {
    name: "猫咪",
    image: `https://cdn.pixabay.com/photo/2021/09/02/16/48/cat-6593947__480.jpg`,
    body: "cat",
  },
  {
    name: "大地",
    image: `https://cdn.pixabay.com/photo/2021/10/11/18/58/lake-6701636__480.jpg`,
    body: "山水",
  },
  {
    name: "绿色",
    image: `https://cdn.pixabay.com/photo/2020/04/24/15/58/white-water-lily-5087465__480.jpg`,
    body: "荷花",
  },
];

export default model;
