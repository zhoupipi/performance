import React, { useEffect, useRef, useState } from "react";
import testPage from './pages/testPage'

export default function DebouceHook(props) {
  const [date, setdate] = useState(new Date());
  const [a, seta] = useState(0);
  // let timer = null;
  let timer = useRef(null)
  // let timerID.current = null;
  let timerID = useRef(null)
  const onChangeHandle = useRef((e) => {
    console.log(timer.current);
    clearTimeout(timer.current);
    timer.current = setTimeout(() => {
      seta(e.target.value);
      console.log("onchange", e, timer.current, timerID.current, date);
    }, 2000);
  });
  console.log('render')
  const tick = useRef(() => {
    setdate(new Date());
  });
  // const tick = () => {
  //   setdate(new Date());
  // };

  useEffect(() => {
    console.log('useeffect')
    timerID.current = setInterval(tick.current, 1000);
    return () => {
      clearInterval(timerID.current);
    };
  }, [tick]);
  console.log("render", date);
  return (
    <>
      <h2>date：{date.toLocaleTimeString()}.</h2>
      <h2>a：{a}</h2>
      <h2>timer：{timer.current}</h2>
      <h2>timerID.current：{timerID.current}</h2>
      <input type="text" onChange={onChangeHandle.current}></input>
      <testPage></testPage>
    </>
  );
}
