import { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import ListComponent from "./components/ListComponent";
import ReactPlaceholder from "react-placeholder/lib";

function App() {
  const [ready, setReady] = useState(false);

  function toggleReady() {
    setReady(!ready);
  }
  return (
    <div className="App">
      <button onClick={toggleReady}>
        toggle "ready" state (ready is "{String(ready)}")
      </button>
      <ReactPlaceholder
        type="media"
        firstLaunchOnly={true}
        ready={ready}
        rows={100}
        color="#E0E0E0"
      >
        <ListComponent />
      </ReactPlaceholder>
    </div>
  );
}

export default App;
