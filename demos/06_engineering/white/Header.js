import { jsx as _jsx, jsxs as _jsxs } from "react/jsx-runtime";
import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
const styles = {
    root: {
        flexGrow: 1,
        backgroundColor: "#317EFB",
    },
    brand: {
        flex: 1,
    },
    about: {
        flex: 1,
        textAlign: "right",
    },
    aboutLink: {
        color: "white",
        textDecoration: "none",
        border: "1px solid white",
        padding: 6,
        margin: "5px",
        borderRadius: 5,
        "&:hover": {
            color: "#f3a279",
            border: "1px solid #f3a279",
        },
        "&:active": {
            color: "#fa6a21",
            border: "1px solid #fa6a21",
        },
    },
};
class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
        };
        this.openDrawer = this.openDrawer.bind(this);
    }
    openDrawer() {
        this.setState({ open: true });
    }
    render() {
        return (_jsx("div", Object.assign({ className: this.props.classes.root }, { children: _jsx(AppBar, Object.assign({ position: "static" }, { children: _jsxs(Toolbar, { children: [_jsxs(Typography, Object.assign({ variant: "h5", color: "inherit", className: this.props.classes.brand }, { children: ["Web Performance Demo", _jsx(Link, Object.assign({ className: this.props.classes.aboutLink, to: "/" }, { children: "Home" }), void 0)] }), void 0), _jsxs(Typography, Object.assign({ variant: "h6", color: "inherit", className: this.props.classes.about }, { children: [_jsx(Link, Object.assign({ className: this.props.classes.aboutLink, to: "/deboucehook" }, { children: "deboucehook" }), void 0), _jsx(Link, Object.assign({ className: this.props.classes.aboutLink, to: "/debouceclass" }, { children: "debouceclass" }), void 0), _jsx(Link, Object.assign({ className: this.props.classes.aboutLink, to: "/about" }, { children: "About" }), void 0)] }), void 0)] }, void 0) }), void 0) }), void 0));
    }
}
export default withStyles(styles)(Header);
