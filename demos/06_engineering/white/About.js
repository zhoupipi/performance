import { jsx as _jsx } from "react/jsx-runtime";
import { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import model from "./model";
import "react-placeholder/lib/reactPlaceholder.css";
import Contact from "./Contact";
const styles = (theme) => ({
    root: {
        display: "flex",
        flexFlow: "column",
        justifyContent: "center",
        alignContent: "center",
        flexWrap: "wrap",
        margin: "2rem",
    },
    title: {
        fontFamily: "Long Cang",
        fontWeight: 400,
        fontStyle: "normal",
        fontSize: "40px",
        lineHeight: "1.40455",
        paddingLeft: 24,
    },
    listContainer: {
        backgroundColor: "#353740",
        border: "2px solid #bcbcbc",
        borderRadius: "20px",
    },
});
class About extends Component {
    render() {
        let contacts = [];
        contacts.push(model.map((m, index) => (_jsx(Contact, { image: m.image, title: m.name, description: m.body }, m.name + index))));
        return (_jsx("main", Object.assign({ className: this.props.classes.root }, { children: contacts }), void 0));
    }
}
export default withStyles(styles)(About);
