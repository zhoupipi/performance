import { jsx as _jsx, jsxs as _jsxs } from "react/jsx-runtime";
import React from "react";
import { createTheme, MuiThemeProvider } from "@material-ui/core/styles";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from "./Header";
import Home from "./Home";
import DebouceClass from "./DebouceClass";
import DebouceHook from "./DebouceHook";
import loadable from "@loadable/component";
const primary = "#1451c9";
const theme = createTheme({
    palette: {
        primary: {
            main: primary,
            contrastText: "#fff",
        },
        secondary: {
            main: "#000000",
            contrastText: primary,
        },
    },
});
// 使用React-Loadable动态加载组件
const LoadableAbout = loadable(() => import("./About.jsx"), {
    fallback: "<div>loading...</div>",
});
class App extends React.Component {
    constructor(props) {
        super(props);
        // this.calculatePi(1500); // 测试密集计算对性能的影响
        // test(); // 测试函数lazy parsing, eager parsing
    }
    calculatePi(duration) {
        const start = new Date().getTime();
        while (new Date().getTime() < start + duration) {
            // TODO(Dereck): figure out the Math problem
        }
    }
    render() {
        return (_jsx(MuiThemeProvider, Object.assign({ theme: theme }, { children: _jsx(Router, { children: _jsx(Switch, { children: _jsxs(React.Fragment, { children: [_jsx(Header, {}, void 0), _jsx(Route, { exact: true, path: "/", component: Home }, void 0), _jsx(Route, { path: "/about", component: LoadableAbout }, void 0), _jsx(Route, { path: "/debouceclass", component: DebouceClass }, void 0), _jsx(Route, { path: "/deboucehook", component: DebouceHook }, void 0)] }, void 0) }, void 0) }, void 0) }), void 0));
    }
}
export default App;
