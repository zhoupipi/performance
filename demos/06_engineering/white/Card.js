import { jsx as _jsx, jsxs as _jsxs } from "react/jsx-runtime";
import React from "react";
import MaterialUICard from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import "./animation.css";
import { LazyLoadImage } from "react-lazy-load-image-component";
import "react-lazy-load-image-component/src/effects/blur.css";
const styles = (theme) => ({
    root: {
        margin: theme.spacing(1),
        willChange: "transform",
    },
    card: {
        width: 200,
    },
    cardSpinning: {
        width: 200,
        animation: "3s linear 1s infinite running rotate",
    },
    media: {
        height: 150,
        width: 200,
        objectFit: "cover",
    },
});
class MyCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            spinning: false,
        };
    }
    spin = () => {
        this.setState({ spinning: true });
    };
    render() {
        /*根据Spinning进行判断决定是否旋转*/
        let cardClass = this.state.spinning
            ? this.props.classes.cardSpinning
            : this.props.classes.card;
        return (_jsx("div", Object.assign({ className: this.props.classes.root }, { children: _jsxs(MaterialUICard, Object.assign({ className: cardClass, onClick: this.spin }, { children: [_jsx(LazyLoadImage, { className: this.props.classes.media, src: this.props.image, effect: "blur", rel: "preconnect" }, void 0), _jsxs(CardContent, { children: [_jsx(Typography, Object.assign({ gutterBottom: true, variant: "h6", component: "h2" }, { children: this.props.title }), void 0), _jsx(Typography, Object.assign({ component: "div" }, { children: this.props.description }), void 0)] }, void 0), _jsx(CardActions, Object.assign({ className: this.props.classes.actions }, { children: _jsx(Button, Object.assign({ size: "small", color: "primary", variant: "outlined" }, { children: "\u8BF4\u660E\uFF1Axxx" }), void 0) }), void 0)] }), void 0) }), void 0));
    }
}
export default withStyles(styles)(MyCard);
