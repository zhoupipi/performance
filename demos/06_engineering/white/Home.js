import { jsx as _jsx, jsxs as _jsxs } from "react/jsx-runtime";
import React, { Suspense } from "react";
import { withStyles } from "@material-ui/core/styles";
// const Card = lazy(() => import("./Card"));
import model from "./model.js";
import Card from './Card';
const styles = (theme) => ({
    root: {
        display: "flex",
        justifyContent: "center",
        alignContent: "center",
        flexWrap: "wrap",
        marginTop: "2rem",
    },
    title: {
        display: "block",
        width: "100%",
        paddingLeft: 40,
        paddingRight: 40,
        fontFamily: "Long Cang",
        fontWeight: 400,
        fontStyle: "normal",
        lineHeight: "1.40455",
        marginLeft: "auto",
        marginRight: "auto",
        textAlign: "left",
    },
});
// relay
class Home extends React.Component {
    render() {
        let cards = [];
        for (let i = 0; i < 2; i++) {
            cards.push(model.map((item) => (_jsx(Suspense, Object.assign({ fallback: _jsx("div", { children: "\u6B63\u5728\u52A0\u8F7D..." }, item.name + 'loading') }, { children: _jsx(Card, { image: item.image, title: item.name, route: item.route, description: item.body }, item.name) }), item.name + 'sus'))));
        }
        return (_jsxs("main", Object.assign({ className: this.props.classes.root }, { children: [_jsx("title", Object.assign({ className: this.props.classes.title }, { children: _jsx("span", { children: "\u6F14\u793A\u4E00\u4E0B\u5B57\u4F53" }, void 0) }), void 0), cards] }), void 0));
    }
}
export default withStyles(styles)(Home);
