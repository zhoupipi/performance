import { jsxs as _jsxs, jsx as _jsx, Fragment as _Fragment } from "react/jsx-runtime";
import { useEffect, useRef, useState } from "react";
export default function DebouceHook(props) {
    const [date, setdate] = useState(new Date());
    const [a, seta] = useState(0);
    let timer = null;
    // let timer = useRef(null)
    let timerID = null;
    // let timerID = useRef(null)
    const onChangeHandle = useRef((e) => {
        console.log(timer);
        clearTimeout(timer);
        timer = setTimeout(() => {
            seta(e.target.value);
            console.log("onchange", e, timer, timerID, date);
        }, 2000);
    });
    console.log('render');
    const tick = useRef(() => {
        setdate(new Date());
    });
    // const tick = () => {
    //   setdate(new Date());
    // };
    useEffect(() => {
        console.log('useeffect');
        timerID = setInterval(tick.current, 1000);
        return () => {
            clearInterval(timerID);
        };
    }, [tick]);
    console.log("render", date);
    return (_jsxs(_Fragment, { children: [_jsxs("h2", { children: ["date\uFF1A", date.toLocaleTimeString(), "."] }, void 0), _jsxs("h2", { children: ["a\uFF1A", a] }, void 0), _jsxs("h2", { children: ["timer\uFF1A", timer] }, void 0), _jsxs("h2", { children: ["timerID\uFF1A", timerID] }, void 0), _jsx("input", { type: "text", onChange: onChangeHandle.current }, void 0), _jsx("testPage", {}, void 0)] }, void 0));
}
