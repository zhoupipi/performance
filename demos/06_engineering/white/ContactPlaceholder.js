import { jsx as _jsx, jsxs as _jsxs } from "react/jsx-runtime";
import React from "react";
import MaterialUICard from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import "./animation.css";
import "react-lazy-load-image-component/src/effects/blur.css";
import CardMedia from "@material-ui/core/CardMedia";
const styles = (theme) => ({
    root: {
        margin: theme.spacing(1),
    },
    card: {
        width: 300,
        height: 300,
    },
    media: {
        height: 200,
        width: 300,
        objectFit: "cover",
        backgroundColor: "#aaa",
        animation: "react-placeholder-pulse 1.5s infinite",
    },
    textBlock: {
        maxHeight: "14.2857%",
        width: "97%",
        height: "1em",
        backgroundColor: "rgb(205, 205, 205)",
        marginTop: 0,
        animation: "react-placeholder-pulse 1.5s infinite",
    },
});
class ContactPlaceholder extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (_jsxs(MaterialUICard, Object.assign({ className: this.props.classes.card }, { children: [_jsx(CardMedia, { className: this.props.classes.media, src: this.props.image }, void 0), _jsxs(CardContent, { children: [_jsx(Typography, Object.assign({ gutterBottom: true, variant: "h6", component: "h2" }, { children: _jsx("div", { className: this.props.classes.textBlock }, void 0) }), void 0), _jsx(Typography, Object.assign({ component: "div" }, { children: _jsx("div", { className: this.props.classes.textBlock }, void 0) }), void 0)] }, void 0)] }), void 0));
    }
}
export default withStyles(styles)(ContactPlaceholder);
