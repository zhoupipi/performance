import { jsx as _jsx, jsxs as _jsxs } from "react/jsx-runtime";
import { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import MaterialUICard from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import ReactPlaceholder from "react-placeholder";
import ContactPlaceholder from "./ContactPlaceholder";
const styles = (theme) => ({
    root: {
        margin: theme.spacing(1),
    },
    card: {
        width: 300,
        height: 300,
    },
    media: {
        height: 200,
        width: 300,
        objectFit: "cover",
    },
});
class Contact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ready: false,
        };
    }
    becomeReady = () => {
        setTimeout(() => {
            this.setState({
                ready: true,
            });
        }, 2000);
    };
    render() {
        const { ready } = this.state;
        const imageStyle = !ready ? { display: "none" } : {};
        let cardMedia = (_jsx(CardMedia, { component: "img", style: imageStyle, className: this.props.classes.media, image: this.props.image, onLoad: this.becomeReady }, void 0));
        return (_jsxs("div", Object.assign({ className: this.props.classes.root }, { children: [_jsx(ReactPlaceholder, Object.assign({ ready: this.state.ready, customPlaceholder: _jsx(ContactPlaceholder, {}, void 0) }, { children: _jsxs(MaterialUICard, Object.assign({ className: this.props.classes.card }, { children: [cardMedia, _jsxs(CardContent, { children: [_jsx(Typography, Object.assign({ gutterBottom: true, variant: "h6", component: "h2" }, { children: this.props.title }), void 0), _jsx(Typography, Object.assign({ component: "div" }, { children: this.props.description }), void 0)] }, void 0)] }), void 0) }), void 0), !ready && cardMedia] }), void 0));
    }
}
export default withStyles(styles)(Contact);
