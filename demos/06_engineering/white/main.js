import { jsx as _jsx } from "react/jsx-runtime";
import React from "react";
import ReactDOM from "react-dom";
import App from "./App.jsx";
import * as _ from "lodash";
import moment from "moment";
import "./main.css";
import { add } from './hashtest/add';
import { sub } from './hashtest/sub';
// function App:any(){
//   return null
// }
const addResult = add(11 + 22);
const subResult = sub(33 - 44);
console.log(addResult, subResult);
console.log(_.join([addResult, subResult], ':'));
let root = document.getElementById("root");
if (root?.hasChildNodes()) {
    ReactDOM.hydrate(_jsx("button", {}, void 0), root);
}
else {
    ReactDOM.render(_jsx(React.StrictMode, { children: _jsx(App, {}, void 0) }, void 0), root);
}
_.add(1, 3);
console.log(moment.isDate);
moment.locale('zh-cn');
const date = moment().format('LL');
console.log(date);
