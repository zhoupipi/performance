import { jsxs as _jsxs, jsx as _jsx, Fragment as _Fragment } from "react/jsx-runtime";
import { Component } from "react";
export default class DebouceTestClass extends Component {
    constructor(props) {
        super(props);
        this.state = { date: new Date(), a: "init" };
        this.timer;
    }
    componentDidMount() {
        this.timerID = setInterval(() => this.tick(), 1000);
    }
    componentWillUnmount() {
        clearInterval(this.timerID);
    }
    tick() {
        this.setState({
            date: new Date(),
        });
    }
    onChangeHandle = (e) => {
        console.log(this.timer);
        clearTimeout(this.timer);
        this.timer = setTimeout(() => {
            this.setState({
                a: e.target.value,
            });
            console.log("onchange", e, this.timer, this.timerID, this.state.date);
        }, 2000);
    };
    render() {
        console.log("render", this.state.date);
        return (_jsxs(_Fragment, { children: [_jsxs("h2", { children: ["date\uFF1A", this.state.date.toLocaleTimeString(), "."] }, void 0), _jsxs("h2", { children: ["a\uFF1A", this.state.a] }, void 0), _jsxs("h2", { children: ["timer\uFF1A", this.timer] }, void 0), _jsxs("h2", { children: ["timerID\uFF1A", this.timerID] }, void 0), _jsx("input", { type: "text", onChange: this.onChangeHandle }, void 0)] }, void 0));
    }
}
