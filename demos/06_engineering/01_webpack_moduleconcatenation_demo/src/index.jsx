import { cube } from "./math.js";
import {a} from './combine'
// 没有从 src/math.js 模块中 import 另外一个 square 方法。这个函数就是所谓的“未引用代码(dead code)”，打包后有 unused 
function component() {
  const element = document.createElement("pre");

  const p =  new Person()
  const s =  new Student()
  
  // Lodash, now imported by this script
  element.innerHTML = ["Hello webpack!", "5 cubed is equal to " + cube(5)].join(
    a() + p.say()+s.say()
  );

  return element;
}

document.body.appendChild(component());

class Person{
  say(){
    return 'person say'
  }
}
class Student{
  say(){
    return 'student say'
  }
}

