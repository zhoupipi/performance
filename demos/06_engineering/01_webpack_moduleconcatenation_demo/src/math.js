function sideEffect(){
    console.log('sideEffect')
}

export function square(x) {
  return x * x;
}

export function cube(x) {
  return x * x * x;
}

