import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
// import UseRefDemo from './UseRefDemo'

ReactDOM.render(
  <React.StrictMode>
    <App />
    {/* <UseRefDemo /> */}
  </React.StrictMode>,
  document.getElementById('root')
)
