import { useState, useCallback, useRef, useEffect, useMemo } from "react";

function C1({ name, children }) {
  function handleC1() {
    console.log('handleC1',name,children)
    return name+'by handleC1'
  }

  // const nameByhandleC1 = handleC1(name)
  const nameByhandleC1ByRef = useRef(handleC1)
  const ucb = useCallback(
    () => {
      console.log('callback')
      handleC1()
    },
    [name]
  )
  const umemo = useMemo(() => {
    console.log('memo')
    return handleC1()
  }, [name])

  return (
    <div className="c1">
      {name}<br/>
      {children}<br/>
      {/* {nameByhandleC1ByRef.current()}<br/> */}
      {ucb()}<br/>
      {umemo}<br/>
    </div>
  );
}

function Parent() {
  const [name, setname]: [String, Function] = useState("name1");
  const [age, setage]: [String, Function]  = useState("99");

  const myUseCallback = useCallback(
    (element) => {
      console.log(element,'element')
    },
    [],
  )

  return (
    <div className="Parent" id="scrollArea">
      <button onClick={() => setname(new Date().getTime())}>改name</button>
      <button onClick={() => setage(new Date().getTime())}>改age</button>
      <div ref={myUseCallback}>need ref</div>
      <C1 name={name}>{age}</C1>
    </div>
  );
}

export default Parent;
