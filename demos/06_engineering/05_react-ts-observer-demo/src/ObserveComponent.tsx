import React from "react";
import { useState, useCallback, useRef, useEffect } from "react";
import useInView from "./hooks/useInView";

type MyProps = {
  index: number;
  item: Number;
  handleObserver: (index: number, inView: boolean) => void;
};

function ObserveComponent({ index, item, handleObserver }: MyProps) {
  const { ref, inView } = useInView();

  useEffect(() => {
    handleObserver(index, inView);
    return () => {
      console.log("ObserveComponent unmount");
    };
  }, [inView]);

  return (
    <div ref={ref} key={index} className="listItem">
      {index}-{">"}
      {item}
    </div>
  );
}

export default React.memo(ObserveComponent);
