const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  swSrc: resolve('../sw.js'),
  swDest: resolve('../dist/sw.js'),
  globDirectory: resolve('../dist'),
  globPatterns: ['../src/*.js']
}
