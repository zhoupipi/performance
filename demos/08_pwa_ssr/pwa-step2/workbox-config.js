module.exports = {
	globDirectory: 'src/',
	globPatterns: [
		'**/*.{ts,css}'
	],
	ignoreURLParametersMatching: [
		/^utm_/,
		/^fbclid$/,
		/^ufo/
	],
	swDest: 'src/sw.js'
};