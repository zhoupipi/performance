var CACHE_NAME = 'my-site-cache-v1';
var urlsToCache = [
  '/demo-sw.html',
  '/static/svg2.svg'
];

self.addEventListener('install', event => {
  console.log('V1 installing…');

  // cache a cat SVG
  event.waitUntil(
    caches.open('static-v1').then(cache => {
      console.log('Opened cache', cache);
      return cache.addAll(urlsToCache);
    })
  );
});

self.addEventListener('activate', event => {
  console.log('V1 now ready to handle fetches!');
});

self.addEventListener('fetch', event => {
  console.log('fetch', event.request.url)
  const url = new URL(event.request.url);

  // serve the cat SVG from the cache if the request is
  // same-origin and the path is '/svg1.svg'
  if (url.origin == location.origin && url.pathname == '/static/svg1.svg') {
    event.respondWith(caches.match('/static/svg2.svg'));
  }
});