// self.process = {
//   env: {
//     NODE_ENV: "production",
//   },
// };
importScripts('https://storage.googleapis.com/workbox-cdn/releases/6.1.1/workbox-sw.js')

workbox.loadModule('workbox-precaching')
workbox.loadModule('workbox-routing')
workbox.loadModule('workbox-strategies')
workbox.loadModule('workbox-expiration')
workbox.loadModule('workbox-cacheable-response')

console.log(workbox)

const { cleanupOutdatedCaches, precacheAndRoute } = workbox.precaching
const { registerRoute } = workbox.routing
const { CacheFirst, StaleWhileRevalidate, NetworkFirst,NetworkOnly } = workbox.strategies
const { ExpirationPlugin } = workbox.expiration
const { CacheableResponsePlugin } = workbox.cacheableResponse

const DAY_IN_SECONDS = 24 * 60 * 60
const MONTH_IN_SECONDS = DAY_IN_SECONDS * 30

// precache
cleanupOutdatedCaches()
const assetsToCache = self.__WB_MANIFEST
// console.log(self.__WB_MANIFEST)
// console.log(self.__WB_DISABLE_DEV_LOGS)
// console.log(self.__dirname)
// console.log(self.__filename)
// precacheAndRoute(assetsToCache)

// routes
// registerRoute(/\.(?:js|css)$/, new StaleWhileRevalidate())
// registerRoute(
//   /\.(?:png|gif|jpg|jpeg|svg)$/,
//   new CacheFirst({
//     cacheName: 'images-cache',
//     plugins: [
//       new ExpirationPlugin({
//         maxEntries: 250,
//         maxAgeSeconds: MONTH_IN_SECONDS
//       })
//     ]
//   })
// )
// Cache page navigations (html) with a Network First strategy
// registerRoute(
//   // Check to see if the request is a navigation to a new page
//   ({ request }) => {
//     return request.mode === 'navigate'
//   },
//   // Use a Network First caching strategy
//   new NetworkFirst({
//     // Put all cached files in a cache named 'pages'
//     cacheName: 'pages',
//     plugins: [
//       // Ensure that only requests that result in a 200 status are cached
//       new CacheableResponsePlugin({
//         statuses: [200],
//       }),
//     ],
//   }),
// );


registerRoute(
  ({url}) => {
    console.log('url-------',url.pathname.includes('/bridge-sw'))
    return url.pathname.includes('/bridge-sw')
  },
  // new RegExp('\/bridge-sw[.]js[?]v=\d*'),
  // '/assets/net.js',
  new NetworkOnly()
);


// Cache CSS, JS, and Web Worker requests with a Stale While Revalidate strategy
registerRoute(
  // Check to see if the request's destination is style for stylesheets, script for JavaScript, or worker for web worker
  ({ request }) => {
    return request.destination === 'style' ||
      request.destination === 'script' ||
      request.destination === 'worker'
  },
  // Use a Stale While Revalidate caching strategy
  new StaleWhileRevalidate({
    // Put all cached files in a cache named 'assets'
    cacheName: 'assets',
    plugins: [
      // Ensure that only requests that result in a 200 status are cached
      new CacheableResponsePlugin({
        statuses: [200],
      }),
    ],
  }),
);

// Cache images with a Cache First strategy
registerRoute(
  // Check to see if the request's destination is style for an image
  ({ request }) => request.destination === 'image',
  // Use a Cache First caching strategy
  new CacheFirst({
    // Put all cached files in a cache named 'images'
    cacheName: 'images',
    plugins: [
      // Ensure that only requests that result in a 200 status are cached
      new CacheableResponsePlugin({
        statuses: [200],
      }),
      // Don't cache more than 50 items, and expire them after 30 days
      new ExpirationPlugin({
        maxEntries: 2,
        maxAgeSeconds: 5, 
      }),
    ],
  }),
);


// self.addEventListener('message', (event) => {
//   if (event && event.data) {
//     console.debug(`Skipping waiting...`, event.data)
//     if (event.data === 'SKIP_WAITING') {
//       console.debug(`Skipping waiting...`)
//       self.skipWaiting()
//       self.clients.claim()
//     }
//   }
// })

