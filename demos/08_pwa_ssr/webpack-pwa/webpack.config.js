const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const webpack = require('webpack'); //to access built-in plugins
const path = require('path');
const { InjectManifest,GenerateSW } = require('workbox-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'index.bundle.js'
  },
  module: {
    rules: [{ test: /\.txt$/, use: 'raw-loader' }],
  },
  plugins: [
    
    // new GenerateSW({
    //   // Do not precache images
    //   exclude: [/\.(?:png|jpg|jpeg|svg)$/],

    //   // Define runtime caching rules.
    //   runtimeCaching: [{
    //     // Match any request that ends with .png, .jpg, .jpeg or .svg.
    //     urlPattern: /\.(?:png|jpg|jpeg|svg)$/,

    //     // Apply a cache-first strategy.
    //     handler: 'CacheFirst',

    //     options: {
    //       // Use a custom cache name.
    //       cacheName: 'images',

    //       // Only cache 10 images.
    //       expiration: {
    //         maxEntries: 10,
    //       },
    //     },
    //   }],
    // }),
    new InjectManifest({
      swSrc: './src/ufo.js',
      swDest: 'service-worker.js',
      // Any other config if needed.
    }),
    new HtmlWebpackPlugin({ template: './src/index.html' })
  ],
};