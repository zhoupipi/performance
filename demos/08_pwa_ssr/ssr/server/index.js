const express = require('express')
const Vue = require('vue')

const app = express()
const renderer = require('vue-server-renderer').createRenderer()
const vue = new Vue({})


const page = new Vue({
    data:{
        title:'my title'
    },
    template: '<div>hello {{title}} </div>'
})


app.get('/', async (req, res) => {
    try {
        const html = await renderer.renderToStream(page)
        res.send(html)
        
    } catch (error) {
        res.status(500).send('fail')
    }
})

app.listen(3000,function(){
    console.log('start success')
})