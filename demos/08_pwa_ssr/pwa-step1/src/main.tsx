import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)

if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {
    navigator.serviceWorker.register('/swufo5.js').then(function(registration) {
      // Registration was successful
      console.log('ServiceWorker registration successful with scope: ', registration.scope);
    }, function(err) {
      // registration failed :(
      console.log('ServiceWorker registration failed:11 ', err);
    });

    navigator.serviceWorker.oncontrollerchange=function(event){
      console.log('页面已经更新')
    }
    if(!navigator.onLine){
      console.log('现在是 offeline')
      this.window.addEventListener('online',event=>{
        console.log('网络已经连接，刷新')
      })
    }
  });
}
